# [0.2.0](https://gitlab.com/spio-wyin/wyin-sdk-feed/compare/v0.1.1...v0.2.0) (2021-11-06)


### Bug Fixes

* **converters:** valid time format is HH:MM ([7b563e2](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/7b563e2f63194a2d16d2924117ff8cd08f6367ab))


### Features

* opt for throwing exceptions ([d9622a5](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/d9622a5bd98b42719095844bdabdd73e4736af1d))



## [0.1.1](https://gitlab.com/spio-wyin/wyin-sdk-feed/compare/v0.1.0...v0.1.1) (2021-11-03)


### Bug Fixes

* allow cross-site requests from the browser [[#11](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/11)] ([fbcf2d1](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/fbcf2d1bc67e761644dcd18f0b64841d361f42bc))
* implement NotFoundModel schema to all SDK exposed functions [[#14](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/14)] ([64db602](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/64db602a233035a09619e60ad0ed415aba990a01))
* improve condition for checking future year [[#15](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/15)] ([4c46bd2](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/4c46bd2c249d8652662908a1c93b24781e31d3dd))
* improve year validation [[#12](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/12)] ([4458edb](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/4458edb70b365480b6e33a69d3c7601928e381d8))


### Performance Improvements

* wait only for century response [[#13](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/13)] ([31fbafd](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/31fbafd75303e6f0d92ca11fc6fb19fc51972848))



# 0.1.0 (2021-10-29)


### Features

* port code that queries wikidata api [[#8](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/8)] ([437cc43](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/437cc43d9f8077d2433eeb24987da24dbd07fe5e))
* port code that queries wikipedia api [[#6](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/6)] ([74e1112](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/74e1112e19df3f7722949c30e3e67b16da5ccabf))
* port html scrapers from wyin-be-feed [[#2](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/2)] ([25bfaba](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/25bfaba5f0ca3239a751ea14983850d7c17af0df))
* port time and year converters [[#7](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/7)] ([d18d981](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/d18d981182034a40bf3e3ffec6523f81e7fddbd2))
* public interface of SDK [[#9](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/9)] ([bc593ac](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/bc593ac323fdc1142461c8e4c292abdc4ec24a6c))
* transfer contracts from wyin-be-feed [[#1](https://gitlab.com/spio-wyin/wyin-sdk-feed/issues/1)] ([3fd738d](https://gitlab.com/spio-wyin/wyin-sdk-feed/commit/3fd738d4e9c00727edb238932c98aacac6d4ab8e))



